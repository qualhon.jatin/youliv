<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/notes';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login(Request $request){
        // echo "<pre>";
        // print_r($request->all());
        // die("hello");
        $validatedData = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $userdata = array(
            'email' => $request->email,
            'password' => $request->password     
        );
        
        if (Auth::attempt($userdata)) {
            return redirect('property_list');
        } else {        
            return redirect('login')->withErrors(['msg' =>'Invalid email address or password']);;
        }
    }
    public function logout(){
        Auth::logout();
        return redirect('login');
    }
}
