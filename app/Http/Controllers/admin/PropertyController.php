<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Property;
use DataTables;



class PropertyController extends Controller
{
    //
    public function __construct(){

    }
    public function add_property(){
        $states = DB::select('select * from state');
        $states =  json_decode(json_encode($states), true);
        return view("dashboard.admin.add_property",['states'=>$states]);
    }
    public function get_cities($id){
        $cities = DB::select("select * from city where state_id = $id ");
        $cities =  json_decode(json_encode($cities), true);
        return response()->json($cities);
    }
    public function submit_property(Request $request){
        
        // echo "<pre>";
        // print_r($request->all());

        // die("hello");

        $validatedData = $request->validate([
            'property_name' => 'required',
            'address' => 'required',
            'sector' => 'required',
            'state' => 'required',
            'city' => 'required',
            'pincode' => 'required|numeric',
            'owner_name' => 'required',
            'property_age' => 'required|numeric',
            'property_type' => 'required'
        ]);
        
        $uploadedImage = "";
        if(!empty($request->property_image)){
            $imageName = $request->property_name. '_' . time() .'.'.request()->property_image->getClientOriginalExtension();
            $path = 'assets/img/property_images';
            request()->property_image->move(public_path($path), $imageName);   
            $uploadedImage =  url($path.'/'.$imageName);   
        }
        
        
        $city_name = getCity($request->city);
        $city_name_sub = strtoupper(substr($city_name,0,3));

        $last_property_data = DB::table('property')->orderBy('id', 'DESC')->first();
        $last_property_data =  json_decode(json_encode($last_property_data), true);

        $property_id = $city_name_sub . '000' . $last_property_data['id'];

       
        $property = new Property();
        $property->property_id = $property_id;
        $property->property_name = $request->property_name;
        $property->owner_name = $request->owner_name;
        $property->property_image = $uploadedImage;
        $property->property_age = $request->property_age;
        $property->property_type = $request->property_type;
        $property->address = $request->address;
        $property->lat = $request->lat;
        $property->lng = $request->lng;
        $property->sector = $request->sector;
        $property->country = "1";
        $property->state = $request->state;
        $property->city = $request->city;
        $property->pincode = $request->pincode;
        $property->landmark = $request->landmark;
        $property->locality1 = $request->locality1;
        $property->locality2 = $request->locality2;
        $property->locality3 = $request->locality3;
        $property->business_model = $request->business_model;
        $property->property_handover_date_revenue = $request->property_handover_date_revenue;
        $property->deal = $request->deal;
        $property->owner_investment = $request->owner_investment;
        $property->investment_details_owner = $request->investment_details_owner;
        $property->youliv_investment = $request->youliv_investment;
        $property->investment_details_youliv = $request->investment_details_youliv;
        $property->payouts = $request->payouts;
        $property->property_handover_date_leased = $request->property_handover_date_leased;
        $property->monthly_rent = $request->monthly_rent;
        $property->security_deposit = $request->security_deposit;
        $property->lease_start_date = $request->lease_start_date;
        $property->lease_term = $request->lease_term;
        $property->annual_rent_increase = $request->annual_rent_increase;
        $property->item_provided_by_owner = $request->item_provided_by_owner;
        $property->property_total_area = $request->property_total_area;
        $property->total_floors = $request->total_floors;
        $property->total_bedrooms = $request->total_bedrooms;
        $property->total_kitchens = $request->total_kitchens;
        $property->total_store_room = $request->total_store_room;
        $property->total_washrooms = $request->total_washrooms;
        $property->total_rooms_with_almirah = $request->total_rooms_with_almirah;
        $property->total_rooms_without_almirah = $request->total_rooms_without_almirah;
        $property->total_rooms_with_kitchen_with_washroom = $request->total_rooms_with_kitchen_with_washroom;
        $property->total_rooms_with_washroom_without_kitchen = $request->total_rooms_with_washroom_without_kitchen;
        $property->total_rooms_with_kitchen_without_washroom = $request->total_rooms_with_kitchen_without_washroom;
        $property->total_rooms_without_kitchen_without_washroom = $request->total_rooms_without_kitchen_without_washroom;
        $property->electricity_meter = $request->electricity_meter;
        $property->sub_meter = $request->sub_meter;
        $property->water_connection = $request->water_connection;
        $property->water_tank_capacity = $request->water_tank_capacity;
        $property->water_tank_quantity = $request->water_tank_quantity;
        $property->borewell = $request->borewell;
        $property->parking_area = $request->parking_area;
        $property->parking_area_value = $request->parking_area_value;
        $property->common_area = $request->common_area;
        $property->common_area_value = $request->common_area_value;
        $property->dining_area = $request->dining_area;
        $property->dining_area_value = $request->dining_area_value;
        $property->washing_area = $request->washing_area;
        $property->washing_area_value = $request->washing_area_value;

        $property->save();
        
        return redirect('/property_list');
        
    }
    public function property_list(){
        //$properties = Property::get()->toArray();
       
        return view("dashboard.admin.property_list");
    }
    public function property_list_data(){
        $properties = Property::orderBy('id', 'desc')->get()->toArray();
        $data = [];
        foreach($properties as $property){
            $property['property_type_view'] = "";
            if($property['property_type'] == 1){
                $property['property_type_view'] = "House/Villa";
            }elseif($property['property_type'] == 2){
                $property['property_type_view'] = "Apartment";
            }
            $property['complete_address'] = $property['address'] . ', ' . $property['sector'] . ', ' . getCity($property['city']) . ', ' . getState($property['state']) . ', ' .$property['pincode'] . ', ' . getCountry($property['country']) . ", Landmark :" . $property['landmark'] ;
            $data[] = $property;
        }

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = '<a href="property_detail/'. $row['id'] .'" class="edit btn btn-primary-custom btn-sm">Manage</a>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
    public function property_detail($id){
        $property_info = Property::where('id',$id)->get()->toArray();
        return view("dashboard.admin.property_detail",['property_info'=>$property_info[0]]);
    }

}
