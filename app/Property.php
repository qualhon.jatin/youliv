<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    //

    protected $table = 'property';
    protected $fillable = [
        'property_id',
        'property_name',
        'address',
        'lat',
        'lng',
        'Sector',
        'country',
        'state',
        'city',
        'pincode',
        'landmark',
    ];
}
