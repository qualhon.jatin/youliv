<?php
    if(!function_exists('getCountry')){
        function getCountry($country_id){
            if($country_id == "1"){
                return "India";
            }
        }
    }
    if(!function_exists('getState')){
        function getState($state_id){
            $state = DB::table('state')->where('id',$state_id)->first();
            if(!empty($state->name)){
                return $state->name;
            }
            
        }
    }
    if(!function_exists('getCity')){
        function getCity($city_id){
            $city = DB::table('city')->where('id',$city_id)->first();
            if(!empty($city->city_name)){
                return $city->city_name;
            }
            
        }
    }
    
?>