<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('property_id')->unique();;
            $table->string('property_name');
            $table->string('address');
            $table->string('lat');
            $table->string('lng');
            $table->string('sector');
            $table->integer('country');
            $table->integer('state');
            $table->integer('city');
            $table->integer('pincode');
            $table->string('landmark')->nullable();;
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property');
    }
}
