@extends('dashboard.base')

@section('content')
<style>
.alert-danger {
    color: #772b35;
    background-color: #fff;
    border-color: #fff;
}
.alert {
    padding: 5px;
}
#myMap {
   height: 350px;
}
#map {
        height: 400px;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }

      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content {
        display: inline;
      }

      .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
      }


</style>    

<div class="container-fluid">
    <div class="fade-in">
        <!-- /.row-->
        <form class="form-horizontal" action="submit_property" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>General Details</strong>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="property_name" class="label">Property name</label>
                                    <span class="required">*</span>
                                    <input class="form-control" id="property_name" type="text" name="property_name" placeholder="Enter property name" value="{{old('property_name')}}">
                                    @error('property_name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="owner_name" class="label">Owner Name</label>
                                    <span class="required">*</span>
                                    <input class="form-control" id="owner_name" type="text" name="owner_name" placeholder="Enter owner name" value="{{old('owner_name')}}">
                                    @error('owner_name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label  class="label">Property Type</label>
                                    <span class="required">*</span><br>
                                    <div class="form-check form-check-inline mr-1">
                                        <input class="form-check-input" id="house" type="radio" value="1" name="property_type" {{ old('property_type')== 1 ? 'checked' : ''  }}>
                                        <label class="form-check-label" for="house">House/Villa</label>
                                    </div>
                                    <div class="form-check form-check-inline mr-1">
                                        <input class="form-check-input" id="apartment" type="radio" value="2" name="property_type" {{ old('property_type')== 2 ? 'checked' : ''  }}>
                                        <label class="form-check-label" for="apartment">Apartment</label>
                                    </div>
                                    @error('property_type')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="property_age" class="label">Property Age</label>
                                    <span class="required">*</span>
                                    <input class="form-control" id="property_age" type="number" name="property_age" placeholder="Enter property age" value="{{old('property_age')}}">
                                    @error('property_age')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="property_image" class="label">Property Image</label>
                                    <input  class="form-control input-image" id="property_image" type="file" name="property_image">
                                    @error('property_image')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                     </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Address Details</strong>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label  for="address" class="label">Street</label>
                                    <span class="required">*</span>
                                    <input class="form-control" id="address" type="text" name="address" placeholder="Enter street address" value="{{old('address')}}">
                                    @error('address')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div> 
                                <div class="col-md-6">
                                    <label  for="sector" class="label">Sector</label>
                                    <span class="required">*</span>
                                    <input class="form-control" id="Enter sector " type="text" name="sector" placeholder="Ex. Sec-22" value="{{old('sector')}}">
                                    @error('sector')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div> 
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="country" class="label">Country</label>
                                    <select class="form-control" id="country" name="country" disabled="">
                                        <option value="0">Select Country</option>
                                        <option value="1" selected>India</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="state" class="label">State</label>
                                    <span class="required">*</span>
                                    <select class="form-control" id="state" name="state">
                                        <option value="">Select State</option>
                                        @foreach ($states as $state)
                                        <option value="{{$state['id']}}" {{ old('state')== $state['id'] ? 'selected' : ''  }} >{{$state['name']}}</option>
                                        @endforeach
                                    </select>
                                    @error('state')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="city" class="label">City</label>
                                    <span class="required">*</span>
                                    <select class="form-control" id="city" name="city">
                                        <option value="">Select City</option>
                                    </select>
                                    @error('city')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="pincode" class="label">Pincode</label>
                                    <span class="required">*</span>
                                    <input class="form-control" id="pincode" type="text" name="pincode" placeholder="Ex. 160022" value="{{old('pincode')}}">
                                    @error('pincode')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="landmark" class="label">Landmark</label>
                                    <input class="form-control" id="landmark" type="text" name="landmark" placeholder="Enter landmark" value="{{old('landmark')}}">
                                    @error('landmark')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="locality1" class="label">Locality 1</label>
                                    <input class="form-control" id="locality1" type="text" name="locality1" placeholder="Enter Locality " value="{{old('locality1')}}">
                                    @error('locality1')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                            <div class="form-group row">
                            <div class="col-md-6">
                                    <label for="locality2" class="label">Locality 2</label>
                                    <input class="form-control" id="locality2" type="text" name="locality2" placeholder="Enter Locality" value="{{old('locality2')}}">
                                    @error('locality2')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                                <div class="col-md-6">
                                    <label for="locality3" class="label">Locality 3</label>
                                    <input class="form-control" id="locality3" type="text" name="locality3" placeholder="Enter Locality" value="{{old('locality3')}}">
                                    @error('locality3')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Business Model :</strong>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label  class="label">Select Type</label>
                                    <span class="required">*</span> : 
                                    <div class="form-check form-check-inline mr-1">
                                        <input class="form-check-input" id="revenue_sharing" type="radio" value="1" name="business_model" {{ old('business_model')== 1 ? 'checked' : ''  }}>
                                        <label class="form-check-label" for="revenue_sharing">Revenue Sharing</label>
                                    </div>
                                    <div class="form-check form-check-inline mr-1">
                                        <input class="form-check-input" id="leased" type="radio" value="2" name="business_model" {{ old('business_model')== 2 ? 'checked' : ''  }}>
                                        <label class="form-check-label" for="leased">Leased</label>
                                    </div>
                                    @error('business_model')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div id="revenue_sharing_section" style="display:none">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="property_handover_date_revenue"  class="label">Date of Property Handover</label>
                                    <input class="form-control" id="property_handover_date_revenue" type="date" name="property_handover_date_revenue" placeholder="date"  value="{{old('property_handover_date_revenue')}}">
                                    @error('property_handover_date_revenue')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="deal" class="label">Deal</label>
                                    <input class="form-control" id="deal" type="number" name="deal" placeholder="Enter deal" value="{{old('deal')}}">
                                    @error('deal')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="owner_investment"  class="label">Owner investment</label>
                                    <input class="form-control" id="owner_investment" type="number" name="owner_investment" placeholder="Enter owner investment" value="{{old('owner_investment')}}">
                                    @error('owner_investment')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="investment_details_owner" class="label">Owner investment Details </label>
                                    <input class="form-control" id="investment_details_owner" type="text" name="investment_details_owner" placeholder="Enter investment details" value="{{old('investment_details_owner')}}">
                                    @error('investment_details_owner')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="youliv_investment"  class="label">Youliv investment</label>
                                    <input class="form-control" id="youliv_investment" type="number" name="youliv_investment" placeholder="Enter youliv investment" value="{{old('youliv_investment')}}">
                                    @error('youliv_investment')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="investment_details_youliv" class="label">Youliv investment Details </label>
                                    <input class="form-control" id="investment_details_youliv" type="text" name="investment_details_youliv" placeholder="Enter investment details" value="{{old('investment_details_youliv')}}">
                                    @error('investment_details_youliv')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                <label for="payouts" class="label">Payouts </label>
                                    <input class="form-control" id="payouts" type="text" name="payouts" placeholder="Enter payouts" value="{{old('payouts')}}">
                                    @error('payouts')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                            </div>
                            <div id="leased_section" style="display:none">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="property_handover_date_leased"  class="label">Date of Property Handover</label>
                                    <input class="form-control" id="property_handover_date_leased" type="date" name="property_handover_date_leased" placeholder="date" value="{{old('property_handover_date_leased')}}">
                                    @error('property_handover_date_leased')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="monthly_rent" class="label">Monthly rent</label>
                                    <input class="form-control" id="monthly_rent" type="number" name="monthly_rent" placeholder="Enter monthly rent" value="{{old('monthly_rent')}}">
                                    @error('monthly_rent')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="security_deposit"  class="label">Security deposit</label>
                                    <input class="form-control" id="security_deposit" type="number" name="security_deposit" placeholder="Enter security deposit"  value="{{old('security_deposit')}}">
                                    @error('security_deposit')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="lease_start_date"  class="label">Lease start date</label>
                                    <input class="form-control" id="lease_start_date" type="date" name="lease_start_date" placeholder="date"  value="{{old('lease_start_date')}}">
                                    @error('lease_start_date')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="lease_term"  class="label">Lease term</label>
                                    <input class="form-control" id="lease_term" type="text" name="lease_term" placeholder="Enter lease term"  value="{{old('lease_term')}}">
                                    @error('lease_term')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="annual_rent_increase"  class="label">Annual rent increase</label>
                                    <input class="form-control" id="annual_rent_increase" type="annual_rent_increase" name="annual_rent_increase" placeholder="Enter annual rent increase"  value="{{old('annual_rent_increase')}}">
                                    @error('annual_rent_increase')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="item_provided_by_owner"  class="label">List of item provided by owner</label>
                                    <input class="form-control" id="item_provided_by_owner" type="text" name="item_provided_by_owner" placeholder="Enter list of item provided by owner"  value="{{old('item_provided_by_owner')}}">
                                    @error('item_provided_by_owner')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Property Description</strong>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="property_total_area" class="label">Total Area of property</label>
                                    <input class="form-control" id="property_total_area" type="number" name="property_total_area" placeholder="Enter total property area" value="{{old('property_total_area')}}">
                                    @error('property_total_area')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                                <div class="col-md-6">
                                    <label for="total_floors" class="label">Number of Floors</label>
                                    <input class="form-control" id="total_floors" type="number" name="total_floors" placeholder="Enter number of floors" value="{{old('total_floors')}}">
                                    @error('total_floors')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="total_bedrooms" class="label">Number of Bedrooms</label>
                                    <input class="form-control" id="total_bedrooms" type="number" name="total_bedrooms" placeholder="Enter number  of bedrooms" value="{{old('total_bedrooms')}}">
                                    @error('total_bedrooms')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                                <div class="col-md-6">
                                    <label for="total_kitchens" class="label">Number of Kitchens</label>
                                    <input class="form-control" id="total_kitchens" type="number" name="total_kitchens" placeholder="Enter number of kitchens" value="{{old('total_kitchens')}}">
                                    @error('total_kitchens')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                            <div class="form-group row">
                                
                                <div class="col-md-6">
                                    <label for="total_store_room" class="label">Number of Store Rooms</label>
                                    <input class="form-control" id="total_store_room" type="number" name="total_store_room" placeholder="Enter number of store rooms" value="{{old('total_store_room')}}">
                                    @error('total_store_room')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                                <div class="col-md-6">
                                    <label for="total_washrooms" class="label">Number of Washrooms</label>
                                    <input class="form-control" id="total_washrooms" type="number" name="total_washrooms" placeholder="Enter number of washrooms" value="{{old('total_washrooms')}}">
                                    @error('deatotal_washroomsl')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                            <div class="form-group row">
                                
                                <div class="col-md-6">
                                    <label for="total_rooms_with_almirah" class="label">No. rooms type with almirahs</label>
                                    <input class="form-control" id="total_rooms_with_almirah" type="number" name="total_rooms_with_almirah" placeholder="Enter No. rooms type with almirahs" value="{{old('total_rooms_with_almirah')}}">
                                    @error('total_rooms_with_almirah')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                                <div class="col-md-6">
                                    <label for="total_rooms_without_almirah" class="label">No. rooms type without almirahs</label>
                                    <input class="form-control" id="total_rooms_without_almirah" type="number" name="total_rooms_without_almirah" placeholder="Enter no. rooms type without almirahs" value="{{old('total_rooms_without_almirah')}}">
                                    @error('total_rooms_without_almirah')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="total_rooms_with_kitchen_with_washroom" class="label">No. of room with attached washroom and kitchen</label>
                                    <input class="form-control" id="total_rooms_with_kitchen_with_washroom" type="number" name="total_rooms_with_kitchen_with_washroom" placeholder="Enter no. of room with attached washroom and kitchen" value="{{old('total_rooms_with_kitchen_with_washroom')}}">
                                    @error('total_rooms_with_kitchen_with_washroom')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                                <div class="col-md-6">
                                    <label for="total_rooms_with_washroom_without_kitchen" class="label">No. of room with attached washroom and without kitchen</label>
                                    <input class="form-control" id="total_rooms_with_washroom_without_kitchen" type="number" name="total_rooms_with_washroom_without_kitchen" placeholder="Enter no. of room with attached washroom and without kitchen" value="{{old('total_rooms_with_washroom_without_kitchen')}}">
                                    @error('total_rooms_with_washroom_without_kitchen')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                            <div class="form-group row">
                                
                                <div class="col-md-6">
                                    <label for="total_rooms_with_kitchen_without_washroom" class="label">No. of room with attached kitchen and without washroom</label>
                                    <input class="form-control" id="total_rooms_with_kitchen_without_washroom" type="number" name="total_rooms_with_kitchen_without_washroom" placeholder="Enter of room with attached kitchen and without washroom" value="{{old('total_rooms_with_kitchen_without_washroom')}}">
                                    @error('total_rooms_with_kitchen_without_washroom')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                                <div class="col-md-6">
                                    <label for="total_rooms_without_kitchen_without_washroom" class="label">No. of room without attached washroom and kitchen</label>
                                    <input class="form-control" id="total_rooms_without_kitchen_without_washroom" type="number" name="total_rooms_without_kitchen_without_washroom" placeholder="Enter no. of room without attached washroom and kitchen" value="{{old('total_rooms_without_kitchen_without_washroom')}}">
                                    @error('total_rooms_without_kitchen_without_washroom')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label  class="label">Electricity meter</label>
                                    <br>
                                    <div class="form-check form-check-inline mr-1">
                                        <input class="form-check-input" id="electricity_meter_yes" type="radio" value="1" name="electricity_meter" {{ old('electricity_meter')== 1 ? 'checked' : ''  }}>
                                        <label class="form-check-label" for="electricity_meter_yes">Yes</label>
                                    </div>
                                    <div class="form-check form-check-inline mr-1">
                                        <input class="form-check-input" id="electricity_meter_no" type="radio" value="2" name="electricity_meter" {{ old('electricity_meter')== 2 ? 'checked' : ''  }}>
                                        <label class="form-check-label" for="electricity_meter_no">no</label>
                                    </div>
                                    @error('electricity_meter')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label  class="label">Separate sub-meters</label>
                                    <br>
                                    <div class="form-check form-check-inline mr-1">
                                        <input class="form-check-input" id="sub_meter_yes" type="radio" value="1" name="sub_meter" {{ old('sub_meter')== 1 ? 'checked' : ''  }}>
                                        <label class="form-check-label" for="sub_meter_yes">Yes</label>
                                    </div>
                                    <div class="form-check form-check-inline mr-1">
                                        <input class="form-check-input" id="sub_meter_no" type="radio" value="2" name="sub_meter" {{ old('sub_meter')== 2 ? 'checked' : ''  }}>
                                        <label class="form-check-label" for="sub_meter_no">no</label>
                                    </div>
                                    @error('sub_meter')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label  class="label">Water connection</label>
                                    <br>
                                    <div class="form-check form-check-inline mr-1">
                                        <input class="form-check-input" id="water_connection_yes" type="radio" value="1" name="water_connection" {{ old('water_connection')== 1 ? 'checked' : ''  }}>
                                        <label class="form-check-label" for="water_connection_yes">Yes</label>
                                    </div>
                                    <div class="form-check form-check-inline mr-1">
                                        <input class="form-check-input" id="water_connection_no" type="radio" value="2" name="water_connection" {{ old('water_connection')== 2 ? 'checked' : ''  }}>
                                        <label class="form-check-label" for="water_connection_no">no</label>
                                    </div>
                                    @error('water_connection')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                                <div class="col-md-6">
                                    <label for="water_tank_capacity" class="label">Water tanks capacity</label>
                                    <input class="form-control" id="water_tank_capacity" type="number" name="water_tank_capacity" placeholder="Enter water tanks capacity" value="{{old('water_tank_capacity')}}" disabled="">
                                    @error('water_tank_capacity')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                <label for="water_tank_quantity" class="label">Water tanks quantity</label>
                                    <input class="form-control" id="water_tank_quantity" type="number" name="water_tank_quantity" placeholder="Enter water tanks quantity" value="{{old('water_tank_quantity')}}" disabled="">
                                    @error('water_tank_quantity')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                                <div class="col-md-6">
                                <label  class="label">Bore-well</label>
                                    <br>
                                    <div class="form-check form-check-inline mr-1">
                                        <input class="form-check-input" id="borewell_yes" type="radio" value="1" name="borewell" {{ old('borewell')== 1 ? 'checked' : ''  }}> 
                                        <label class="form-check-label" for="borewell_yes">Yes</label>
                                    </div>
                                    <div class="form-check form-check-inline mr-1">
                                        <input class="form-check-input" id="borewell_no" type="radio" value="2" name="borewell" {{ old('borewell')== 2 ? 'checked' : ''  }}>
                                        <label class="form-check-label" for="borewell_no">no</label>
                                    </div>
                                    @error('borewell')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label  class="label">Parking area</label>
                                    <br>
                                    <div class="form-check form-check-inline mr-1">
                                        <input class="form-check-input" id="parking_area_yes" type="radio" value="1" name="parking_area" {{ old('parking_area')== 1 ? 'checked' : ''  }}>
                                        <label class="form-check-label" for="parking_area_yes">Yes</label>
                                    </div>
                                    <div class="form-check form-check-inline mr-1">
                                        <input class="form-check-input" id="parking_area_no" type="radio" value="2" name="parking_area" {{ old('parking_area')== 2 ? 'checked' : ''  }}>
                                        <label class="form-check-label" for="parking_area_no">no</label>
                                    </div>
                                    @error('parking_area')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                                <div class="col-md-6">
                                    <label for="parking_area_value" class="label">Parking area</label>
                                    <input class="form-control" id="parking_area_value" type="number" name="parking_area_value" placeholder="Enter parking area" value="{{old('parking_area_value')}}" disabled="">
                                    @error('parking_area_value')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label  class="label">Common area</label>
                                    <br>
                                    <div class="form-check form-check-inline mr-1">
                                        <input class="form-check-input" id="common_area_yes" type="radio" value="1" name="common_area" {{ old('common_area')== 1 ? 'checked' : ''  }}>
                                        <label class="form-check-label" for="common_area_yes">Yes</label>
                                    </div>
                                    <div class="form-check form-check-inline mr-1">
                                        <input class="form-check-input" id="common_area_no" type="radio" value="2" name="common_area" {{ old('common_area')== 2 ? 'checked' : ''  }}>
                                        <label class="form-check-label" for="common_area_no">no</label>
                                    </div>
                                    @error('common_area')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                                <div class="col-md-6">
                                    <label for="common_area_value" class="label">Common area</label>
                                    <input class="form-control" id="common_area_value" type="number" name="common_area_value" placeholder="Enter parking area" value="{{old('common_area_value')}}" disabled="">
                                    @error('common_area_value')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label  class="label">Dining area</label>
                                    <br>
                                    <div class="form-check form-check-inline mr-1">
                                        <input class="form-check-input" id="dining_area_yes" type="radio" value="1" name="dining_area" {{ old('dining_area')== 1 ? 'checked' : ''  }}>
                                        <label class="form-check-label" for="dining_area_yes">Yes</label>
                                    </div>
                                    <div class="form-check form-check-inline mr-1">
                                        <input class="form-check-input" id="dining_area_no" type="radio" value="2" name="dining_area" {{ old('dining_area')== 2 ? 'checked' : ''  }}>
                                        <label class="form-check-label" for="dining_area_no">no</label>
                                    </div>
                                    @error('dining_area')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                                <div class="col-md-6">
                                    <label for="dining_area_value" class="label">Dining area</label>
                                    <input class="form-control" id="dining_area_value" type="number" name="dining_area_value" placeholder="Enter dining area" value="{{old('dining_area_value')}}" disabled="">
                                    @error('dining_area_value')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label  class="label">Washing area</label>
                                    <br>
                                    <div class="form-check form-check-inline mr-1">
                                        <input class="form-check-input" id="washing_area_yes" type="radio" value="1" name="washing_area" {{ old('washing_area')== 1 ? 'checked' : ''  }}>
                                        <label class="form-check-label" for="washing_area_yes">Yes</label>
                                    </div>
                                    <div class="form-check form-check-inline mr-1">
                                        <input class="form-check-input" id="washing_area_no" type="radio" value="2" name="washing_area" {{ old('washing_area')== 2 ? 'checked' : ''  }}>
                                        <label class="form-check-label" for="washing_area_no">no</label>
                                    </div>
                                    @error('washing_area')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                                <div class="col-md-6">
                                    <label for="washing_area_value" class="label">Washing area</label>
                                    <input class="form-control" id="washing_area_value" type="number" name="washing_area_value" placeholder="Enter washing area" value="{{old('washing_area_value')}}" disabled="">
                                    @error('washing_area_value')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row-->
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Location Details</strong>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-md-12 map-div">  
                                    <input id="lat" type="hidden" name="lat" value="" /> 
                                    <input id="lng" type="hidden" name="lng" value="" /> 
                                    <label for="address">Please pick a location for this property </label>
                                    <span class="required">*</span>
                                    <div class="pac-card" id="pac-card">
                                        <div>
                                            <div id="title">
                                                Autocomplete search
                                            </div>
                                            <div id="type-selector" class="pac-controls">
                                                <input type="radio" name="type" id="changetype-all" checked="checked">
                                                <label for="changetype-all">All</label>

                                                <input type="radio" name="type" id="changetype-establishment">
                                                <label for="changetype-establishment">Establishments</label>

                                                <input type="radio" name="type" id="changetype-address">
                                                <label for="changetype-address">Addresses</label>

                                                <input type="radio" name="type" id="changetype-geocode">
                                                <label for="changetype-geocode">Geocodes</label>
                                            </div>
                                            <div id="strict-bounds-selector" class="pac-controls">
                                                <input type="checkbox" id="use-strict-bounds" value="">
                                                <label for="use-strict-bounds">Strict Bounds</label>
                                            </div>
                                        </div>
                                        <div id="pac-container">
                                            <input id="pac-input" type="text"
                                                placeholder="Enter a location">
                                        </div>
                                    </div>
                                    <div id="map"></div>
                                    <div id="infowindow-content">
                                        <img src="" width="16" height="16" id="place-icon">
                                        <span id="place-name"  class="title"></span><br>
                                        <span id="place-address"></span>
                                    </div>

                                </div> 
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="hidden" id="add_property_page">;
                            <button class="btn btn-primary-custom" type="submit"> Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


@endsection

@section('javascript')

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/app.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-bFgxDKJ8bxAgLYyn2sVJEvWtAzr4qCc&libraries=places" async defer></script>




@endsection