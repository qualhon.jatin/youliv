@extends('dashboard.base')

@section('content')
<style>
.alert-danger {
    color: #772b35;
    background-color: #fff;
    border-color: #fff;
}
.alert {
    padding: 5px;
}


</style>    

<div class="container-fluid">
    <div class="fade-in">
        <!-- /.row-->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}  
                        <div class="card-header"><strong>Add Room Details</strong></div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="price" class="label">Price</label>
                                        <span class="required">*</span>
                                        <input class="form-control" id="price" type="text" name="price" placeholder="Enter price" value="{{old('price')}}">
                                        @error('price')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label  for="description" class="label">Description</label>
                                        <span class="required">*</span>
                                        <input class="form-control" id="description" type="text" name="description" placeholder="Enter description" value="{{old('description')}}">
                                        @error('description')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div> 
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="country" class="label">Amenities</label>
                                        <span class="required">*</span><br>
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="tv" type="checkbox" value="1" name="amenties[]">
                                            <label class="form-check-label" for="tv">TV</label>
                                        </div>
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="wifi" type="checkbox" value="2" name="amenties[]">
                                            <label class="form-check-label" for="wifi">Wifi</label>
                                        </div>
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="parking" type="checkbox" value="3" name="amenties[]">
                                            <label class="form-check-label" for="parking">Parking</label>
                                        </div>
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="cooler" type="checkbox" value="4" name="amenties[]">
                                            <label class="form-check-label" for="cooler">Cooler</label>
                                        </div>
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="geyser" type="checkbox" value="5" name="amenties[]">
                                            <label class="form-check-label" for="geyser">Geyser</label>
                                        </div>
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="almirah" type="checkbox" value="6" name="amenties[]">
                                            <label class="form-check-label" for="almirah">Almirah</label>
                                        </div>
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="ac" type="checkbox" value="7" name="amenties[]">
                                            <label class="form-check-label" for="ac">AC</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="state" class="label">PG Type</label>
                                        <span class="required">*</span><br>
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="girls" type="radio" value="1" name="pg_type">
                                            <label class="form-check-label" for="girls">Girls</label>
                                        </div>
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="boys" type="radio" value="2" name="pg_type">
                                            <label class="form-check-label" for="boys">Boys</label>
                                        </div>
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="unisex" type="radio" value="3" name="pg_type">
                                            <label class="form-check-label" for="unisex">Unisex</label>
                                        </div>
                                        @error('state')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="city" class="label">Room Type</label>
                                        <span class="required">*</span><br>
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="single_occupency" type="radio" value="1" name="room_type">
                                            <label class="form-check-label" for="single_occupency">Single occupancy</label>
                                        </div>
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="double_occupency" type="radio" value="2" name="room_type">
                                            <label class="form-check-label" for="double_occupency">Double occupancy</label>
                                        </div>
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="triple_occupency" type="radio" value="3" name="room_type">
                                            <label class="form-check-label" for="triple_occupency">Triple occupancy</label>
                                        </div>
                                        @error('city')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                    <label for="property_image" class="label">Floor</label>
                                        <span class="required">*</span><br>
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="ground" type="radio" value="1" name="room_type">
                                            <label class="form-check-label" for="ground">Ground</label>
                                        </div>
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="first" type="radio" value="2" name="room_type">
                                            <label class="form-check-label" for="first">First</label>
                                        </div>
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="second" type="radio" value="1" name="room_type">
                                            <label class="form-check-label" for="second">Second</label>
                                        </div>
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="third" type="radio" value="2" name="room_type">
                                            <label class="form-check-label" for="third">Third</label>
                                        </div>
                                        @error('property_image')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="landmark" class="label">Food</label>
                                        <span class="required">*</span>
                                        <br>
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="with_food" type="radio" value="1" name="room_type">
                                            <label class="form-check-label" for="with_food">With food</label>
                                        </div>
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="without_food" type="radio" value="2" name="room_type">
                                            <label class="form-check-label" for="without_food">Without food</label>
                                        </div>
                                        @error('landmark')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label for="address"></label>
                                        <div id="div1" style="border:1px solid #aaaaaa;text-align:center;padding:10px"  ondrop="drop(event)" ondragover="allowDrop(event)">
                                        <span>Drag and Drop Images here <br>
                                        OR<br>
                                        <input id="file-input" type="file" name="file-input">
                                        </span>
                                    </div> 
                                        @error('address')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div> 
                                </div>
                            </div>
                            
                            <div class="card-footer">
                                <button class="btn btn-primary-custom" type="button"> Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        <!-- /.row-->
        </div>
    </div>
</div>


@endsection

@section('javascript')


@endsection