@extends('dashboard.base')

@section('content')

    
<div class="container-fluid">
    <div class="fade-in">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><i class="fa fa-align-justify"></i> <span style="font-size:25px;">Property detail</span> 
                        <div class="col-6 col-sm-4 col-md-3  mb-3 mb-xl-0" style="float:right;">
                            <button class="btn btn-block btn-primary-custom" type="button"><a style="color:#fff" href="{{url('add_room',['id'=>$property_info['id']])}}">Add new room +</a></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <img src="{{$property_info['property_image']}}" width="400px">
                            </div>
                            <div class="col-md-6">
                                <h2>{{$property_info['property_name']}} ({{$property_info['property_id']}}) </h2>
                                <div><strong>Property owner : </strong>{{$property_info['owner_name']}}</div>
                                <div>
                                    <strong>Property type : </strong>
                                    @if($property_info['property_type'] == 1)
                                        House/Villa
                                    @elseif($property_info['property_type'] == 2)
                                        Apartment
                                    @endif
                                </div>
                                <div><strong>Property age : </strong>{{$property_info['property_age']}}</div><br>
                                <h4>Address :</h4>
                                <div>
                                    {{$property_info['address']}} , 
                                    {{$property_info['sector']}} , 
                                    {{getCity($property_info['city'])}} , 
                                    {{getState($property_info['state'])}} , 
                                    {{$property_info['pincode']}} , 
                                    {{getCountry($property_info['country'])}} , 
                                    Landmark : {{$property_info['landmark']}}
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top:25px;">
                            <!--<div class="col-md-12 map-div-view-property">  
                                <input id="lat" type="hidden" name="lat" value="{{$property_info['lat']}}" /> 
                                <input id="lng" type="hidden" name="lng" value="{{$property_info['lng']}}" /> 
                                <label for="address">Location for this property </label>
                                <div id="map_canvas" style="height: 350px;"></div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Business Model :</strong>
                            @if($property_info['business_model'] == 1)
                                Revenue Sharing
                            @elseif($property_info['business_model'] == 2)
                                Leased
                            @endif
                        </div>
                        <div class="card-body">
                            @if($property_info['business_model'] == 1)
                            <div id="revenue_sharing_section">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="property_handover_date_revenue"  class="label">Date of Property Handover</label><br>
                                    @if($property_info['property_handover_date_revenue'])
                                        {{$property_info['property_handover_date_revenue']}}
                                    @else
                                        N/A
                                    @endif    

                                    
                                </div>
                                <div class="col-md-6">
                                    <label for="deal" class="label">Deal</label><br>
                                    @if($property_info['deal'])
                                        {{$property_info['deal']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="owner_investment"  class="label">Owner investment</label><br>
                                    @if($property_info['owner_investment'])
                                        {{$property_info['owner_investment']}}
                                    @else
                                        N/A
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label for="investment_details_owner" class="label">Owner investment Details </label><br>
                                    @if($property_info['investment_details_owner'])
                                        {{$property_info['investment_details_owner']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="youliv_investment"  class="label">Youliv investment</label><br>
                                    @if($property_info['youliv_investment'])
                                        {{$property_info['youliv_investment']}}
                                    @else
                                        N/A
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label for="investment_details_youliv" class="label">Youliv investment Details </label><br>
                                    @if($property_info['investment_details_youliv'])
                                        {{$property_info['investment_details_youliv']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="payouts" class="label">Payouts </label><br>
                                    @if($property_info['payouts'])
                                        {{$property_info['payouts']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                            </div>
                            </div>
                            @endif
                            @if($property_info['business_model'] == 2)
                            <div id="leased_section" >
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="property_handover_date_leased"  class="label">Date of Property Handover</label><br>
                                    @if($property_info['property_handover_date_leased'])
                                        {{$property_info['property_handover_date_leased']}}
                                    @else
                                        N/A
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label for="monthly_rent" class="label">Monthly rent</label><br>
                                    @if($property_info['monthly_rent'])
                                        {{$property_info['monthly_rent']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="security_deposit"  class="label">Security deposit</label><br>
                                    @if($property_info['security_deposit'])
                                        {{$property_info['security_deposit']}}
                                    @else
                                        N/A
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label for="lease_start_date"  class="label">Lease start date</label><br>
                                    @if($property_info['lease_start_date'])
                                        {{$property_info['lease_start_date']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="lease_term"  class="label">Lease term</label><br>
                                    @if($property_info['lease_term'])
                                        {{$property_info['lease_term']}}
                                    @else
                                        N/A
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label for="annual_rent_increase"  class="label">Annual rent increase</label><br>
                                    @if($property_info['annual_rent_increase'])
                                        {{$property_info['annual_rent_increase']}}
                                    @else
                                        N/A
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="item_provided_by_owner"  class="label">List of item provided by owner</label><br>
                                    @if($property_info['item_provided_by_owner'])
                                        {{$property_info['item_provided_by_owner']}}
                                    @else
                                        N/A
                                    @endif
                                </div>
                            </div>
                            <div>
                            @endif
                        </div>
                    </div> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Property Description</strong>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="property_total_area" class="label">Total Area of property</label><br>
                                    @if($property_info['property_total_area'])
                                        {{$property_info['property_total_area']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                                <div class="col-md-6">
                                    <label for="total_floors" class="label">Number of Floors</label><br>
                                    @if($property_info['total_floors'])
                                        {{$property_info['total_floors']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="total_bedrooms" class="label">Number of Bedrooms</label><br>
                                    @if($property_info['total_bedrooms'])
                                        {{$property_info['total_bedrooms']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                                <div class="col-md-6">
                                    <label for="total_kitchens" class="label">Number of Kitchens</label><br>
                                    @if($property_info['total_kitchens'])
                                        {{$property_info['total_kitchens']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                            </div>
                            <div class="form-group row">
                                
                                <div class="col-md-6">
                                    <label for="total_store_room" class="label">Number of Store Rooms</label><br>
                                    @if($property_info['total_store_room'])
                                        {{$property_info['total_store_room']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                                <div class="col-md-6">
                                    <label for="total_washrooms" class="label">Number of Washrooms</label><br>
                                    @if($property_info['total_washrooms'])
                                        {{$property_info['total_washrooms']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                            </div>
                            <div class="form-group row">
                                
                                <div class="col-md-6">
                                    <label for="total_rooms_with_almirah" class="label">No. rooms type with almirahs</label><br>
                                    @if($property_info['total_rooms_with_almirah'])
                                        {{$property_info['total_rooms_with_almirah']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                                <div class="col-md-6">
                                    <label for="total_rooms_without_almirah" class="label">No. rooms type without almirahs</label><br>
                                    @if($property_info['total_rooms_without_almirah'])
                                        {{$property_info['total_rooms_without_almirah']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="total_rooms_with_kitchen_with_washroom" class="label">No. of room with attached washroom and kitchen</label><br>
                                    @if($property_info['total_rooms_with_kitchen_with_washroom'])
                                        {{$property_info['total_rooms_with_kitchen_with_washroom']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                                <div class="col-md-6">
                                    <label for="total_rooms_with_washroom_without_kitchen" class="label">No. of room with attached washroom and without kitchen</label><br>
                                    @if($property_info['total_rooms_with_washroom_without_kitchen'])
                                        {{$property_info['total_rooms_with_washroom_without_kitchen']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                            </div>
                            <div class="form-group row">
                                
                                <div class="col-md-6">
                                    <label for="total_rooms_with_kitchen_without_washroom" class="label">No. of room with attached kitchen and without washroom</label><br>
                                    @if($property_info['total_rooms_with_kitchen_without_washroom'])
                                        {{$property_info['total_rooms_with_kitchen_without_washroom']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                                <div class="col-md-6">
                                    <label for="total_rooms_without_kitchen_without_washroom" class="label">No. of room without attached washroom and kitchen</label><br>
                                    @if($property_info['total_rooms_without_kitchen_without_washroom'])
                                        {{$property_info['total_rooms_without_kitchen_without_washroom']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label  class="label">Electricity meter</label><br>
                                    @if($property_info['electricity_meter'])
                                        {{$property_info['electricity_meter']}}
                                    @else
                                        N/A
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label  class="label">Separate sub-meters</label><br>
                                    @if($property_info['sub_meter'])
                                        {{$property_info['sub_meter']}}
                                    @else
                                        N/A
                                    @endif  
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label  class="label">Water connection</label>
                                    <br>
                                    @if($property_info['water_connection'])
                                        {{$property_info['water_connection']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                                <div class="col-md-6">
                                    <label for="water_tank_capacity" class="label">Water tanks capacity</label><br>
                                    @if($property_info['water_tank_capacity'])
                                        {{$property_info['water_tank_capacity']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="water_tank_quantity" class="label">Water tanks quantity</label><br>
                                    @if($property_info['water_tank_quantity'])
                                        {{$property_info['water_tank_quantity']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                                <div class="col-md-6">
                                    <label  class="label">Bore-well</label><br>
                                    @if($property_info['borewell'])
                                        {{$property_info['borewell']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label  class="label">Parking area</label>
                                    <br>
                                    @if($property_info['parking_area'])
                                        {{$property_info['parking_area']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                                <div class="col-md-6">
                                    <label for="parking_area_value" class="label">Parking area</label><br>
                                    @if($property_info['parking_area_value'])
                                        {{$property_info['parking_area_value']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label  class="label">Common area</label>
                                    <br>
                                    @if($property_info['common_area'])
                                        {{$property_info['common_area']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                                <div class="col-md-6">
                                    <label for="common_area_value" class="label">Common area</label><br>
                                    @if($property_info['common_area_value'])
                                        {{$property_info['common_area_value']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label  class="label">Dining area</label>
                                    <br>
                                    @if($property_info['dining_area'])
                                        {{$property_info['dining_area']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                                <div class="col-md-6">
                                    <label for="dining_area_value" class="label">Dining area</label><br>
                                    @if($property_info['dining_area_value'])
                                        {{$property_info['dining_area_value']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label  class="label">Washing area</label>
                                    <br>
                                    @if($property_info['washing_area'])
                                        {{$property_info['washing_area']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                                <div class="col-md-6">
                                    <label for="washing_area_value" class="label">Washing area</label><br>
                                    @if($property_info['washing_area_value'])
                                        {{$property_info['washing_area_value']}}
                                    @else
                                        N/A
                                    @endif
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
    </div>
</div>

@endsection

@section('javascript')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/app.js') }}"></script>

@endsection