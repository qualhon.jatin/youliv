@extends('dashboard.base')

@section('content')

<style>
  
  
</style>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<div class="container-fluid">
            <div class="fade-in">
              <div class="row">
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header"><i class="fa fa-align-justify"></i> Property List 
                        <div class="col-6 col-sm-4 col-md-3  mb-3 mb-xl-0" style="float:right;">
                            <button class="btn btn-block btn-primary-custom" type="button"><a style="color:#fff" href="add_property">Add New Property</a></button>
                        </div>
                    </div>
                    <div class="card-body">
                    <table class="table table-bordered data-table property-list-data">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Property ID</th>
                          <th>Name of property</th>
                          <th>Name of owner</th>
                          <th>Address</th>
                          <th>Property type</th>
                          <th>Total no. of rooms</th>
                          <th>Assigned to</th>
                          <th width="50px">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                      <!--<ul class="pagination">
                        <li class="page-item"><a class="page-link" href="#">Prev</a></li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                        <li class="page-item"><a class="page-link" href="#">Next</a></li>
                      </ul>-->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

@endsection

@section('javascript')


<script src="assets/js/app.js"></script>


@endsection